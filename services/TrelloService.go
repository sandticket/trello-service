package services

import (
	"strings"
	"time"
	"trello-service/interfaces"
	"trello-service/models"
)

type TrelloService struct {
	interfaces.ITrelloRepository
}

func (service *TrelloService) Import(boardId string) (models.ImportResponse, error) {
	var importResponse models.ImportResponse
	var tasks []models.Task
	trelloLists, err := service.ITrelloRepository.GetLists(boardId)
	if err != nil {
		return importResponse, err
	}
	for _, trelloList := range trelloLists {
		trelloCards := trelloList.Cards
		for _, trelloCard := range trelloCards {
			task := new(models.Task)
			task.Title = trelloCard.Name
			task.DueDate = trelloCard.Due
			task.Description = trelloCard.Desc
			task.CreationDate = time.Now().Format(time.RFC3339)

			if trelloCard.Closed {
				task.Status = "CLOSED"
			} else {
				task.Status = "NOT_CLOSED"
			}

			for index, trelloMemberId := range trelloCard.IdMembers {
				trelloUser, err := service.ITrelloRepository.GetMemberById(trelloMemberId)
				if err != nil {
					return importResponse, err
				}
				nameSlice := strings.Split(trelloUser.FullName, " ")
				var user = new(models.User)
				user.Email = trelloUser.Email
				if len(nameSlice) > 1 {
					user.FirstName = nameSlice[1]
				}
				if len(nameSlice) > 0 {
					user.LastName = nameSlice[0]
				}

				if index == 0 {
					task.Creator = *user
				}
			}
			tasks = append(tasks, *task)
		}
	}
	importResponse.Tasks = tasks
	return importResponse, nil
}
