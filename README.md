# trello-service

This service is a component of the project "Epsi-Ticket-Manager". This component communicated with trello and can import and export trello project through it's API. The details concerning the APIs are in the file "swagger.yml"