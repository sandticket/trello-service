package controller

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"trello-service/interfaces"
)

type TrelloController struct {
	interfaces.ITrelloService
}

func (controller *TrelloController) Import(res http.ResponseWriter, req *http.Request) {
	params := mux.Vars(req)
	boardId := params["boardId"]
	tasks, err := controller.ITrelloService.Import(boardId)
	if err != nil {
		http.Error(res, err.Error(), 500)
		return
	}

	output, err := json.Marshal(tasks)
	if err != nil {
		http.Error(res, err.Error(), 500)
		return
	}
	res.WriteHeader(http.StatusOK)
	_, _ = res.Write(output)
}
