package models

type Project struct {
	Name  string `json:"name"`
	Users []User `json:"users"`
	Tasks []Task `json:"tasks"`
}
