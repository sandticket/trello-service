package models

type TrelloList struct {
	Id    string       `json:"id"`
	Name  string       `json:"name"`
	Cards []TrelloCard `json:"cards"`
}
