package models

type ImportResponse struct {
	Tasks []Task `json:"tasks"`
}
