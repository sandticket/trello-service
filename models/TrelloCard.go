package models

type TrelloCard struct {
	Id        string   `json:"id"`
	Name      string   `json:"name"`
	Closed    bool     `json:"closed"`
	Desc      string   `json:"desc"`
	IdMembers []string `json:"idMembers"`
	Due       string   `json:"due"`
}
