package models

type TrelloMember struct {
	Id       string `json:"id"`
	FullName string `json:"fullName"`
	Email    string `json:"email"`
}
