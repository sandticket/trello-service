package models

type TrelloBoard struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}
