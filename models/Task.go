package models

type Task struct {
	Title        string `json:"title"`
	Description  string `json:"description"`
	CreationDate string `json:"creationDate"`
	DueDate      string `json:"dueDate"`
	UpdateDate   string `json:"lastUpdateDate"`
	Status       string `json:"status"`
	Creator      User   `json:"creator"`
}
