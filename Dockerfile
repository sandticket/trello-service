# STEP 1 build executable binary
FROM golang as builder

COPY . $GOPATH/src/grocery/backend/
WORKDIR $GOPATH/src/grocery/backend/

ENV GO111MODULE=on

#get dependancies
RUN go mod download

#build the binary
RUN CGO_ENABLED=0 GOOS=linux go build -a -o /go/bin/app

# STEP 2 build a small image
# start from busybox
FROM busybox
# Copy our static executable
COPY --from=builder /go/bin/app /app

EXPOSE 8080

CMD ["/app"]
