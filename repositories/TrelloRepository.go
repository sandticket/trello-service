package repositories

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"trello-service/models"
)

type TrelloRepository struct {
	ApiKey        string
	ApiToken      string
	TrelloBaseUrl string
}

func (repository *TrelloRepository) Init() error {
	repository.ApiKey = os.Getenv("TRELLO_API_KEY")
	repository.ApiToken = os.Getenv("TRELLO_API_TOKEN")
	repository.TrelloBaseUrl = os.Getenv("TRELLO_BASE_URL")
	if len(repository.ApiKey) == 0 || len(repository.ApiToken) == 0 || len(repository.TrelloBaseUrl) == 0 {
		return fmt.Errorf("Env varriable TRELLO_API_KEY or TRELLO_API_TOKEN or TRELLO_BASE_URL is empty")
	}
	return nil
}

func (repository *TrelloRepository) GetMemberById(id string) (models.TrelloMember, error) {
	var result models.TrelloMember
	url := fmt.Sprintf("https://%s/1/members/%s?fields=fullName,email&key=%s&token=%s", repository.TrelloBaseUrl, id, repository.ApiKey, repository.ApiToken)
	response, err := http.Get(url)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return result, err
	} else {
		data, err := ioutil.ReadAll(response.Body)
		defer response.Body.Close()
		if err != nil {
			return result, err
		}

		err = json.Unmarshal(data, &result)
		if err != nil {
			return result, err
		}
		return result, nil
	}
}

func (repository *TrelloRepository) GetLists(boardId string) ([]models.TrelloList, error) {
	url := fmt.Sprintf("https://%s/1/boards/%s/lists?cards=all&filter=open&fields=cards&key=%s&token=%s", repository.TrelloBaseUrl, boardId, repository.ApiKey, repository.ApiToken)
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	response, err := http.Get(url)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return nil, err
	}
	data, err := ioutil.ReadAll(response.Body)
	defer response.Body.Close()
	if err != nil {
		return nil, err
	}

	var trelloLists []models.TrelloList
	err = json.Unmarshal(data, &trelloLists)
	if err != nil {
		return nil, err
	}
	return trelloLists, nil
}

func (repository *TrelloRepository) GetBoardNameById(boardId string) (string, error) {
	url := fmt.Sprintf("https://"+repository.TrelloBaseUrl+"/1/boards/%s?fields=name&key=%s&token=%s", boardId, repository.ApiKey, repository.ApiToken)
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	response, err := http.Get(url)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return "", err
	}
	data, err := ioutil.ReadAll(response.Body)
	defer response.Body.Close()
	if err != nil {
		return "", err
	}

	var trelloBoard models.TrelloBoard
	err = json.Unmarshal(data, &trelloBoard)
	if err != nil {
		return "", err
	}
	return trelloBoard.Name, nil
}
