package interfaces

import "trello-service/models"

type ITrelloService interface {
	Import(boardId string) (models.ImportResponse, error)
}
