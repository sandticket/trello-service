package interfaces

import "trello-service/models"

type ITrelloRepository interface {
	Init() error
	GetBoardNameById(boardId string) (string, error)
	GetMemberById(id string) (models.TrelloMember, error)
	GetLists(boardId string) ([]models.TrelloList, error)
}
