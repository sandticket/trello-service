package main

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
	"trello-service/controller"
	"trello-service/repositories"
	"trello-service/services"
)

func main() {
	port := os.Getenv("PORT")
	if len(port) == 0 {
		port = "8080"
	}
	repository := &repositories.TrelloRepository{}
	err := repository.Init()
	if err != nil {
		log.Fatal(err.Error())
	}
	service := &services.TrelloService{repository}
	trelloController := controller.TrelloController{service}
	rtr := mux.NewRouter()
	rtr.HandleFunc("/import/{boardId:[a-z1-9]+}", trelloController.Import).Methods("GET")
	http.Handle("/", rtr)
	log.Print("ready: listening...\n")

	http.ListenAndServe(":" + port, nil)
}
